extends Control

func _ready():
	for btn in get_tree().get_nodes_in_group('btn_ch_lvl'):
		if G.save_lvl < int(btn.text):
			btn.disabled = true


func _on_Btn1_pressed():
	get_tree().change_scene("res://maps/map1.tscn")


func _on_Btn2_pressed():
	get_tree().change_scene("res://maps/map3.tscn")


func _on_Btn3_pressed():
	get_tree().change_scene("res://maps/map2.tscn")
