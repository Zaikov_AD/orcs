extends Area2D

func _on_Area2D_area_shape_entered(area_id, area, area_shape, self_shape):
	# точно такой же методо использовать и для меча
	if area != null: # переменной присвоено значение
	# проверка на то, что ресурс на который ведет переменная 
	# не удаляется и не инициализируется, 
	#а полность готов 
		if is_instance_valid(area):
			if area.name == 'PlayerHitbox':
				G.emit_signal('player_hurt')
				get_parent().queue_free()

func _on_Area2D_body_shape_entered(body_id, body, body_shape, area_shape):
	if body != null: 
		if is_instance_valid(body):
			if body == G.Player:
				get_parent().queue_free()