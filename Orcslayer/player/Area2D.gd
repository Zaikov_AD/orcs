extends Area2D

func _on_Area2D_area_shape_entered(area_id, area, area_shape, self_shape):
	# точно такой же методо использовать и для меча
	if area != null: # переменной присвоено значение
	# проверка на то, что ресурс на который ведет переменная 
	# не удаляется и не инициализируется, 
	#а полность готов 
		if is_instance_valid(area):
			if area.name == 'Hitbox':
				var npc = area.get_parent()
				prints('arrow_hit', npc)
				G.emit_signal('npc_get_shot', npc, G.Player.BASE_ARCH_DMG + G.Player.bonus_dmg_arch)
				# для того чтобы НПС агрился при попадании стрелы, даже если мы не в поле зрения
				_on_Area2D_body_shape_entered(G.Player.get_rid(), G.Player, 1, 1) 
				#
				get_parent().queue_free()


# ЕСли стрела столкнулась с объектом, то уничтожаем ее
func _on_Area2D_body_shape_entered(body_id, body, body_shape, area_shape):
	if body != null:
		if is_instance_valid(body):
			if body != G.Player:
				get_parent().queue_free()