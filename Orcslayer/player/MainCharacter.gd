extends KinematicBody2D

var Arrow = preload("res://player/Arrow.tscn")
var anim = 0
const BASE_SPEED = 120
const INC_SPEED = 5
var CircleOfDeath = preload("res://CircleOfDeath.tscn")

var SPEED = BASE_SPEED
var bonus_dmg_sword = 0
var bonus_dmg_arch = 0
var INC_DAMAGE = 0.5

const BASE_ARCH_DMG = 0.5
const BASE_SWORD_DMG = 1

const FLOOR = Vector2(0, -1)
var direct = "u"
var velocity = Vector2()
var m = Vector2() # mouse pos
var upr = true
var spawn_pos
var num = 0

var health = 6 setget set_health, get_health
var INC_HEALTH = 1
var healthMAX=health
var dead = false
var old_pos # <<<<<<<<<
const DIST_UPD_PATH = 64 # <<<<<<<<<

var inv = 0
var dont_play=0
var blink = 0.25

signal npc_hurt_left
signal npc_hurt_right
signal npc_hurt_down
signal npc_hurt_up
signal deinv

func start():
	SPEED = BASE_SPEED + (INC_SPEED * G.cur_stats["S"])
	self.health = health + (INC_HEALTH * G.cur_stats["H"])
	bonus_dmg_sword = INC_DAMAGE * G.cur_stats["D"]
	bonus_dmg_arch = INC_DAMAGE * 0.5 * G.cur_stats["D"]
	if (G.permissions[G.Permit.left]
			and G.permissions[G.Permit.right] 
			and G.permissions[G.Permit.up] 
			and G.permissions[G.Permit.down]):
		upr = true
	else:
		upr = false


func check_need_upd_path():# <<<<<<<<<
	if old_pos.distance_to(position) > DIST_UPD_PATH: # <<<<<<<<<
		G.emit_signal("upd_path") # <<<<<<<<<
		old_pos = position # <<<<<<<<<


func set_health(val):
	health = val
	G.HUD.reset_lives()
	for i in val:
		G.HUD.add_live_indicator()
	if health <= 0 and !dead:
		dead = true
		G.HUD.dead_scr.visible = true
		get_tree().paused = true


func get_health():
	return health

func reset():
	position = spawn_pos
	dead = false
	self.health = 3

func _ready(): # <<<<<<<<<
	spawn_pos = position
	G.Player = self
	old_pos = position # <<<<<<<<<
	G.connect("upd_path", G, "_on_upd_path") # <<<<<<<<<
	G.connect("player_hurt", self, "_on_player_hurt")
	$"/root/G".connect("destroy", self, "_on_destroy")
	
func _on_player_hurt():
	if inv==0:
		self.health -= G.BASE_NPC_DMG
		$HurtSound.play()
	$Self_Inv_Timer.start()
	inv = 1

func _on_timer_timeout_heal(timer):
	G.permissions[G.Permit.heal] = true
	timer.queue_free()


func _physics_process(delta):
	if !dead:
		if (upr==true):
			move()
		else:
			move2()
		if anim == 0 && Input.is_action_just_pressed("ui_attack") and G.permissions[G.Permit.sword]:
			anim = 1 # меч
		elif anim == 0 &&  Input.is_action_just_pressed("ui_arch") and G.permissions[G.Permit.arch]:
			m = get_viewport().get_mouse_position()
			arch()
		elif anim == 0 && Input.is_action_just_pressed("ui_heal") and G.permissions[G.Permit.heal]:
			anim=3
			heal()
		if inv==1:
			$AnimatedSprite.modulate.a=0.75-blink
			blink = blink*(-1)


func arch():
	var arr = Arrow.instance()
	var x = get_viewport().size.x/2
	var y = get_viewport().size.y/2
	if direct == "u" && y - m.y > abs(x - m.x):
		if velocity.y==0:
			$AnimatedSprite.play("ArchUp")
		else:
			$AnimatedSprite.play("GoArchUp")
		anim=2
		arr.position = position
	elif direct == "l" && x-m.x > abs(y - m.y):
		if velocity.x==0:
			$AnimatedSprite.play("ArchLeft")
		else:
			$AnimatedSprite.play("GoArchLeft")
		anim=2
		arr.position = position
	elif direct == "d" && m.y - y > abs(x-m.x):
		if velocity.y==0:
			$AnimatedSprite.play("ArchDown")
		else:
			$AnimatedSprite.play("GoArchDown")
		anim=2
		arr.position = position
	elif direct == "r" && m.x - x > abs(y-m.y):
		if velocity.x==0:
			$AnimatedSprite.play("ArchRight")
		else:
			$AnimatedSprite.play("GoArchRight")
		anim=2
		arr.position = position
	else:
		pass
	get_parent().add_child(arr)
	var imp = Vector2(m.x-x,m.y-y).normalized()
	arr.rotate(r(imp.y,imp.x))
	arr.apply_impulse(Vector2(),imp*300)
#
#
func r(d,k):
	var i=0
	var a=d
	var deg = d
	while  abs(a)>0.0001:
		a = a*(d*d*(2*i+1)*(2*i+1)/(2*(i+1)*(2*i+3)))
		deg+=a;
		i+=1;
	if k >=0: return deg
	else: return 3.14159265-deg


func move2():
	if Input.is_action_pressed("ui_up") and G.permissions[G.Permit.up]:
		if direct == "u":
			velocity.y = -SPEED
			if (anim==0): $AnimatedSprite.play("GoUp")
			elif (anim == 1): 
				if $AnimatedSprite.animation=="GoSwordUp":
					$AnimatedSprite.play("GoSwordUp")
				else:
					if $AnimatedSprite.animation.match("GoSword*") or $AnimatedSprite.animation.match("Sword*"):
						num = $AnimatedSprite.frame
						$AnimatedSprite.play("GoSwordUp")
						$AnimatedSprite.frame=num
					else:
						$AnimatedSprite.play("GoSwordUp")
			elif (anim==2):
				if $AnimatedSprite.animation=="GoArchUp" or $AnimatedSprite.animation=="GoUp":
					$AnimatedSprite.play("GoArchUp")
				else:
					if $AnimatedSprite.animation=="GoArchDown" or $AnimatedSprite.animation=="GoDown":
						num = $AnimatedSprite.frame
						$AnimatedSprite.play("GoArchUp")
						$AnimatedSprite.frame=num
					else:
						$AnimatedSprite.play("GoArchUp")
		elif direct == "r":
			velocity.x = SPEED
			if (anim==0):
				$AnimatedSprite.play("GoRight")
			elif (anim==1):
				if $AnimatedSprite.animation=="GoSwordRight":
					$AnimatedSprite.play("GoSwordRight")
				else:
						if $AnimatedSprite.animation.match("GoSword*") or $AnimatedSprite.animation.match("Sword*"):
							num = $AnimatedSprite.frame
							$AnimatedSprite.play("GoSwordRight")
							$AnimatedSprite.frame=num
						else:
							$AnimatedSprite.play("GoSwordRight")
			elif (anim==2):
				if $AnimatedSprite.animation=="GoArchRight" or $AnimatedSprite.animation=="GoRight":
					$AnimatedSprite.play("GoArchRight")
				else:
					if $AnimatedSprite.animation=="GoArchDown" or $AnimatedSprite.animation=="GoDown":
						num = $AnimatedSprite.frame
						$AnimatedSprite.play("GoArchRight")
						$AnimatedSprite.frame=num
					else:
						$AnimatedSprite.play("GoArchRight")
		elif direct == "d":
			velocity.y = SPEED
			if (anim==0): 
				$AnimatedSprite.play("GoDown")
			elif (anim==1):
				if $AnimatedSprite.animation=="GoSwordDown":
					$AnimatedSprite.play("GoSwordDown")
				else:
					if $AnimatedSprite.animation.match("GoSword*") or $AnimatedSprite.animation.match("Sword*"):
						num = $AnimatedSprite.frame
						$AnimatedSprite.play("GoSwordDown")
						$AnimatedSprite.frame=num
					else:
						$AnimatedSprite.play("GoSwordDown")
			elif (anim==2):
				if $AnimatedSprite.animation=="GoArchDown" or $AnimatedSprite.animation=="GoDown":
					$AnimatedSprite.play("GoArchDown")
				else:
					if $AnimatedSprite.animation=="GoArchDown" or $AnimatedSprite.animation=="GoDown":
						num = $AnimatedSprite.frame
						$AnimatedSprite.play("GoArchDown")
						$AnimatedSprite.frame=num
					else:
						$AnimatedSprite.play("GoArchDown")
		else:
			velocity.x = -SPEED
			if (anim==0): 
				$AnimatedSprite.play("GoLeft")
			elif (anim==1):
				if $AnimatedSprite.animation=="GoSwordLeft":
					$AnimatedSprite.play("GoSwordLeft")
				else:
					if $AnimatedSprite.animation.match("GoSword*") or $AnimatedSprite.animation.match("Sword*"):
							num = $AnimatedSprite.frame
							$AnimatedSprite.play("GoSwordLeft")
							$AnimatedSprite.frame=num
					else:
						$AnimatedSprite.play("GoSwordLeft")
			elif (anim==2):
				if $AnimatedSprite.animation=="GoArchLeft" or $AnimatedSprite.animation=="GoLeft":
					$AnimatedSprite.play("GoArchLeft")
				else:
					if $AnimatedSprite.animation=="GoArchDown" or $AnimatedSprite.animation=="GoDown":
							num = $AnimatedSprite.frame
							$AnimatedSprite.play("GoArchLeft")
							$AnimatedSprite.frame=num
					else:
						$AnimatedSprite.play("GoArchLeft")
	if Input.is_action_pressed("ui_down") and G.permissions[G.Permit.down]:
		if direct == "u":
			velocity.y = SPEED
			if (anim==0): $AnimatedSprite.play("GoUp")
			elif (anim == 1): 
				if $AnimatedSprite.animation=="GoSwordUp":
					$AnimatedSprite.play("GoSwordUp")
				else:
					if $AnimatedSprite.animation.match("GoSword*") or $AnimatedSprite.animation.match("Sword*"):
						num = $AnimatedSprite.frame
						$AnimatedSprite.play("GoSwordUp")
						$AnimatedSprite.frame=num
					else:
						$AnimatedSprite.play("GoSwordUp")
			elif (anim==2):
				if $AnimatedSprite.animation=="GoArchUp" or $AnimatedSprite.animation=="GoUp":
					$AnimatedSprite.play("GoArchUp")
				else:
					if $AnimatedSprite.animation=="GoArchDown" or $AnimatedSprite.animation=="GoDown":
						num = $AnimatedSprite.frame
						$AnimatedSprite.play("GoArchUp")
						$AnimatedSprite.frame=num
					else:
						$AnimatedSprite.play("GoArchUp")
		elif direct == "r":
			velocity.x=-SPEED
			if (anim==0):
				$AnimatedSprite.play("GoRight")
			elif (anim==1):
				if $AnimatedSprite.animation=="GoSwordRight":
					$AnimatedSprite.play("GoSwordRight")
				else:
					if $AnimatedSprite.animation.match("GoSword*") or $AnimatedSprite.animation.match("Sword*"):
						num = $AnimatedSprite.frame
						$AnimatedSprite.play("GoSwordRight")
						$AnimatedSprite.frame=num
					else:
						$AnimatedSprite.play("GoSwordRight")
			elif (anim==2):
				if $AnimatedSprite.animation=="GoArchRight" or $AnimatedSprite.animation=="GoRight":
					$AnimatedSprite.play("GoArchRight")
				else:
					if $AnimatedSprite.animation.match("GoArch*") or $AnimatedSprite.animation.match("Arch*"):
						num = $AnimatedSprite.frame
						$AnimatedSprite.play("GoArchRight")
						$AnimatedSprite.frame=num
					else:
						$AnimatedSprite.play("GoArchRight")
		elif direct=="d":
			velocity.y = -SPEED
			if (anim==0): 
				$AnimatedSprite.play("GoDown")
			elif (anim==1):
				if $AnimatedSprite.animation=="GoSwordDown":
					$AnimatedSprite.play("GoSwordDown")
				else:
					if $AnimatedSprite.animation.match("GoSword*") or $AnimatedSprite.animation.match("Sword*"):
						num = $AnimatedSprite.frame
						$AnimatedSprite.play("GoSwordDown")
						$AnimatedSprite.frame=num
					else:
						$AnimatedSprite.play("GoSwordDown")
			elif (anim==2):
				if $AnimatedSprite.animation=="GoArchDown" or $AnimatedSprite.animation=="GoDown":
					$AnimatedSprite.play("GoArchDown")
				else:
					if $AnimatedSprite.animation=="GoArchDown" or $AnimatedSprite.animation=="GoDown":
						num = $AnimatedSprite.frame
						$AnimatedSprite.play("GoArchDown")
						$AnimatedSprite.frame=num
					else:
						$AnimatedSprite.play("GoArchDown")
		else:
			velocity.x=SPEED
			if (anim==0): 
				$AnimatedSprite.play("GoLeft")
			elif (anim==1):
				if $AnimatedSprite.animation=="GoSwordLeft":
					$AnimatedSprite.play("GoSwordLeft")
				else:
					if $AnimatedSprite.animation.match("GoSword*") or $AnimatedSprite.animation.match("Sword*"):
						num = $AnimatedSprite.frame
						$AnimatedSprite.play("GoSwordLeft")
						$AnimatedSprite.frame=num
					else:
						$AnimatedSprite.play("GoSwordLeft")
			elif (anim==2):
				if $AnimatedSprite.animation=="GoArchLeft" or $AnimatedSprite.animation=="GoLeft":
					$AnimatedSprite.play("GoArchLeft")
				else:
					if $AnimatedSprite.animation=="GoArchDown" or $AnimatedSprite.animation=="GoDown":
						num = $AnimatedSprite.frame
						$AnimatedSprite.play("GoArchLeft")
						$AnimatedSprite.frame=num
					else:
						$AnimatedSprite.play("GoArchLeft")
	if Input.is_action_just_pressed("ui_right") and G.permissions[G.Permit.right]:
		if direct == "u": direct = "r"
		elif direct == "r": direct = "d"
		elif direct == "d": direct = "l"
		else: direct = "u"
	if Input.is_action_just_pressed("ui_left") and G.permissions[G.Permit.left]:
		if direct == "u": direct = "l"
		elif direct == "r": direct = "u"
		elif direct == "d": direct = "r"
		else: direct = "d"

	if velocity.x==0 && velocity.y==0:
		if direct=="r":
			if (anim==1):
				num = $AnimatedSprite.frame
				$AnimatedSprite.play("SwordRight")
				$AnimatedSprite.frame=num
			elif (anim==2):
				num = $AnimatedSprite.frame
				$AnimatedSprite.play("ArchRight")
				$AnimatedSprite.frame=num
			elif (anim==0):
				$AnimatedSprite.play("StayR")
		if direct=="l":
			if (anim==1):
				num = $AnimatedSprite.frame
				$AnimatedSprite.play("SwordLeft")
				$AnimatedSprite.frame=num
			elif (anim==2):
				num = $AnimatedSprite.frame
				$AnimatedSprite.play("ArchLeft")
				$AnimatedSprite.frame=num
			elif (anim==0):
				$AnimatedSprite.play("StayL")
		if direct=="d":
			if (anim==1):
				num = $AnimatedSprite.frame
				$AnimatedSprite.play("SwordDown")
				$AnimatedSprite.frame=num
			elif (anim==2):
				num = $AnimatedSprite.frame
				$AnimatedSprite.play("ArchDown")
				$AnimatedSprite.frame=num
			elif (anim==0):
				$AnimatedSprite.play("StayD")
		if direct=="u":
			if (anim==1):
				num = $AnimatedSprite.frame
				$AnimatedSprite.play("SwordUp")
				$AnimatedSprite.frame=num
			elif (anim==2):
				num = $AnimatedSprite.frame
				$AnimatedSprite.play("ArchUp")
				$AnimatedSprite.frame=num
			elif (anim==0):
				$AnimatedSprite.play("StayU")
	velocity = move_and_slide(velocity, FLOOR)
	check_need_upd_path() # <<<<
	velocity.x=0
	velocity.y=0

		

func move():
	if Input.is_action_pressed("ui_up"):
		if velocity.y>=0: velocity.y-=SPEED
		direct="u"
		if (anim==0): $AnimatedSprite.play("GoUp")
		elif (anim == 1): 
			if $AnimatedSprite.animation=="GoSwordUp":
				$AnimatedSprite.play("GoSwordUp")
			else:
					if $AnimatedSprite.animation.match("GoSword*") or $AnimatedSprite.animation.match("Sword*"):
						num = $AnimatedSprite.frame
						$AnimatedSprite.play("GoSwordUp")
						$AnimatedSprite.frame=num
					else:
						$AnimatedSprite.play("GoSwordUp")
		elif (anim==2):
			if $AnimatedSprite.animation=="GoArchUp" or $AnimatedSprite.animation=="GoUp":
				$AnimatedSprite.play("GoArchUp")
			else:
				if $AnimatedSprite.animation=="GoArchDown" or $AnimatedSprite.animation=="GoDown":
					num = $AnimatedSprite.frame
					$AnimatedSprite.play("GoArchUp")
					$AnimatedSprite.frame=num
				else:
					$AnimatedSprite.play("GoArchUp")
		else:
			velocity.y=0
#
	elif Input.is_action_pressed("ui_down"):
		if velocity.y<=0: velocity.y+=SPEED
		direct="d"
		if (anim==0): $AnimatedSprite.play("GoDown")
		elif (anim==1):
			if $AnimatedSprite.animation=="GoSwordDown":
				$AnimatedSprite.play("GoSwordDown")
			else:
					if $AnimatedSprite.animation.match("GoSword*") or $AnimatedSprite.animation.match("Sword*"):
						num = $AnimatedSprite.frame
						$AnimatedSprite.play("GoSwordDown")
						$AnimatedSprite.frame=num
					else:
						$AnimatedSprite.play("GoSwordDown")
		elif (anim==2):
			if $AnimatedSprite.animation=="GoArchDown" or $AnimatedSprite.animation=="GoDown":
				$AnimatedSprite.play("GoArchDown")
			else:
				if $AnimatedSprite.animation=="GoArchDown" or $AnimatedSprite.animation=="GoDown":
					num = $AnimatedSprite.frame
					$AnimatedSprite.play("GoArchDown")
					$AnimatedSprite.frame=num
				else:
					$AnimatedSprite.play("GoArchDown")
		else:
			velocity.y=0
#
	elif velocity.y==0 and Input.is_action_pressed("ui_left"):
		if velocity.x>=0: velocity.x=-SPEED
		direct="l"
		if (anim==0): $AnimatedSprite.play("GoLeft")
		elif (anim==1):
			if $AnimatedSprite.animation=="GoSwordLeft":
				$AnimatedSprite.play("GoSwordLeft")
			else:
				if $AnimatedSprite.animation.match("GoSword*") or $AnimatedSprite.animation.match("Sword*"):
					num = $AnimatedSprite.frame
					$AnimatedSprite.play("GoSwordLeft")
					$AnimatedSprite.frame=num
				else:
					$AnimatedSprite.play("GoSwordLeft")
		elif (anim==2):
			if $AnimatedSprite.animation=="GoArchLeft" or $AnimatedSprite.animation=="GoLeft":
				$AnimatedSprite.play("GoArchLeft")
			else:
				if $AnimatedSprite.animation=="GoArchDown" or $AnimatedSprite.animation=="GoDown":
					num = $AnimatedSprite.frame
					$AnimatedSprite.play("GoArchLeft")
					$AnimatedSprite.frame=num
				else:
					$AnimatedSprite.play("GoArchLeft")
		else:
			velocity.x=0
#
	elif velocity.y==0 and Input.is_action_pressed("ui_right"):
		if velocity.x<=0: velocity.x+=SPEED
		direct="r"
		if (anim==0): 
			$AnimatedSprite.play("GoRight")
		elif (anim==1):
			if $AnimatedSprite.animation=="GoSwordRight":
				$AnimatedSprite.play("GoSwordRight")
			else:
				if $AnimatedSprite.animation.match("GoSword*") or $AnimatedSprite.animation.match("Sword*"):
					num = $AnimatedSprite.frame
					$AnimatedSprite.play("GoSwordRight")
					$AnimatedSprite.frame=num
				else:
					$AnimatedSprite.play("GoSwordRight")
		elif (anim==2):
			if $AnimatedSprite.animation=="GoArchRight" or $AnimatedSprite.animation=="GoRight":
				$AnimatedSprite.play("GoArchRight")
			else:
				if $AnimatedSprite.animation=="GoArchDown" or $AnimatedSprite.animation=="GoDown":
					num = $AnimatedSprite.frame
					$AnimatedSprite.play("GoArchRight")
					$AnimatedSprite.frame=num
				else:
					$AnimatedSprite.play("GoArchRight")
		else:
			velocity.x=0

	if velocity.x==0 && velocity.y==0:
		if direct=="r":
			if (anim==1):
				num = $AnimatedSprite.frame
				$AnimatedSprite.play("SwordRight")
				$AnimatedSprite.frame=num
			elif (anim==2):
				num = $AnimatedSprite.frame
				$AnimatedSprite.play("ArchRight")
				$AnimatedSprite.frame=num
			elif (anim==0):
				$AnimatedSprite.play("StayR")
		if direct=="l":
			if (anim==1):
				num = $AnimatedSprite.frame
				$AnimatedSprite.play("SwordLeft")
				$AnimatedSprite.frame=num
			elif (anim==2):
				num = $AnimatedSprite.frame
				$AnimatedSprite.play("ArchLeft")
				$AnimatedSprite.frame=num
			elif (anim==0):
				$AnimatedSprite.play("StayL")
		if direct=="d":
			if (anim==1):
				num = $AnimatedSprite.frame
				$AnimatedSprite.play("SwordDown")
				$AnimatedSprite.frame=num
			elif (anim==2):
				num = $AnimatedSprite.frame
				$AnimatedSprite.play("ArchDown")
				$AnimatedSprite.frame=num
			elif (anim==0):
				$AnimatedSprite.play("StayD")
		if direct=="u":
			if (anim==1):
				num = $AnimatedSprite.frame
				$AnimatedSprite.play("SwordUp")
				$AnimatedSprite.frame=num

			elif (anim==2):
				num = $AnimatedSprite.frame
				$AnimatedSprite.play("ArchUp")
				$AnimatedSprite.frame=num
			elif (anim==0):
				$AnimatedSprite.play("StayU")
	velocity = move_and_slide(velocity, FLOOR)
	velocity.x=0
	velocity.y=0


func _on_AnimatedSprite_animation_finished():
	anim = 0
	emit_signal("deinv")
	dont_play=0

func _on_AnimatedSprite_frame_changed():
	if $AnimatedSprite.animation.match("*SwordLeft") && $AnimatedSprite.frame>1 && inv==0:
		emit_signal("npc_hurt_left", bonus_dmg_sword)
	elif $AnimatedSprite.animation.match("*SwordRight") && $AnimatedSprite.frame>1 && inv==0:
		emit_signal("npc_hurt_right", bonus_dmg_sword)
	elif $AnimatedSprite.animation.match("*SwordDown") && $AnimatedSprite.frame>1 && inv==0:
		emit_signal("npc_hurt_down", bonus_dmg_sword)
	elif $AnimatedSprite.animation.match("*SwordUp") && $AnimatedSprite.frame>1 && inv==0:
		emit_signal("npc_hurt_up", bonus_dmg_sword)
	if $AnimatedSprite.animation.match("*Sword*") && $AnimatedSprite.frame>1:
		if (dont_play==0):
			$SwordSound.play(0.0)
		dont_play=1
	if $AnimatedSprite.animation.match("*Arch*") && $AnimatedSprite.frame>1:
		if (dont_play==0):
			$ArchSound.play(0.0)
		dont_play=1

func _on_ZoneDown_area_shape_entered(area_id, area, area_shape, self_shape):
	if area != null:
		if is_instance_valid(area):
			if area.name == "Hitbox":
				G.emit_signal('npc_get_vul_down', area.get_parent())
func _on_ZoneDown_area_shape_exited(area_id, area, area_shape, self_shape):
	if area != null:
		if is_instance_valid(area):
			if area.name == "Hitbox":
				G.emit_signal('npc_get_inv_down', area.get_parent())
				
				
func _on_ZoneUp_area_shape_entered(area_id, area, area_shape, self_shape):
	if area != null:
		if is_instance_valid(area):
			if area.name == "Hitbox":
				G.emit_signal('npc_get_vul_up', area.get_parent())
func _on_ZoneUp_area_shape_exited(area_id, area, area_shape, self_shape):
	if area != null:
		if is_instance_valid(area):
			if area.name == "Hitbox":
				G.emit_signal('npc_get_inv_up', area.get_parent())

func on_timer_timeout_heal(timer):
	G.permissions[G.Permit.heal] = true
	timer.queue_free()

func heal():
	if direct == "l":
		$AnimatedSprite.play("HealLeft")
		anim=3
	elif direct == "r":
		$AnimatedSprite.play("HealRight")
		anim=3
	elif direct == "d":
		$AnimatedSprite.play("HealDown")
		anim=3
	elif direct == "u":
		$AnimatedSprite.play("HealUp")
		anim=3
	if health < healthMAX:
		self.health+=1
	var h = Timer.new(); h.name = 'healTimeout'
	h.wait_time = 6
	h.connect("timeout",self,"on_timer_timeout_heal", [h])
	add_child(h)
	h.start()
	G.permissions[G.Permit.heal] = false

func _on_ZoneLeft_area_shape_entered(area_id, area, area_shape, self_shape):
	if area != null:
		if is_instance_valid(area):
			if area.name == "Hitbox":
				G.emit_signal('npc_get_vul_left', area.get_parent())
func _on_ZoneLeft_area_shape_exited(area_id, area, area_shape, self_shape):
	if area != null:
		if is_instance_valid(area):
			if area.name == "Hitbox":
				G.emit_signal('npc_get_inv_left', area.get_parent())


func _on_ZoneRight_area_shape_entered(area_id, area, area_shape, self_shape):
	if area != null:
		if is_instance_valid(area):
			if area.name == "Hitbox":
				G.emit_signal('npc_get_vul_right', area.get_parent())
func _on_ZoneRight_area_shape_exited(area_id, area, area_shape, self_shape):
	if area != null:
		if is_instance_valid(area):
			if area.name == "Hitbox":
				G.emit_signal('npc_get_inv_right', area.get_parent())
				
func _on_Self_Inv_Timer_timeout():
	self.inv=0
	$AnimatedSprite.modulate.a=1



func _on_destroy():
	if inv==0:
		self.health -= 1
		$HurtSound.play()
	$Self_Inv_Timer.start()
	inv = 1
