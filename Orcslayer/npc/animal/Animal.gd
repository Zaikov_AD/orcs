extends KinematicBody2D
onready var anim_pl = $AnimationPlayer

const GOAL_DEAD_ZONE = 32
const SPEED = 25 # Скорость
var dir = Vector2(0,0) # Направление

var spawn_pos # точка появления

var cpath # путь (массив
var move = false #двигается

var patrool_pos


func reset():
	position = spawn_pos
	cpath = null
	move = false
	dir = Vector2(0,0)
	#anim_pl.play("stay_f")

func _ready():
	spawn_pos = position
	add_to_group("animals")


func anim():
	dir = dir.round()
	if dir == Vector2(-1, 0):
		anim_pl.play('left')
	elif dir == Vector2(1, 0):
		anim_pl.play('right')
	elif dir == Vector2(0, -1):
		anim_pl.play('back')
	elif dir == Vector2(0, 1):
		anim_pl.play('forward')
	#else: # анимация при простое
	#	if anim_sprite.animation == 'left':
	#		anim_sprite.play('stay_l')
	#	elif anim_sprite.animation == 'right':
	#		anim_sprite.play('stay_r')
	#	elif anim_sprite.animation == 'forward':
	#		anim_sprite.play('stay_f')
	#	elif anim_sprite.animation == 'back':
	#		anim_sprite.play('stay_b')


func go_to_point(pos):
	if cpath == null: # путь не существует - создаем
		cpath = G.ASM.get_point_path(
				G.ASM.get_closest_point(Vector3(position.x, position.y, 0)),
				G.ASM.get_closest_point(Vector3(pos.x, pos.y, 0))
		)
		cpath.remove(0) # удаляем точку под своим тайлом, чтобы при начале движения не дрожать
		if cpath == PoolVector3Array():
			cpath = null
		G.debug_draw_path(self, cpath)
	else: # путь существует
		var d = floor(position.distance_to(Vector2(cpath[0].x, cpath[0].y))) # floor - округление к меньшему
		#print(d)
		if not(d == 0): # дистанция до ближайщей точки в пути не = 0 // P --{d}-> [o]ooooooooX
			# Движемся к точке
			dir = position.direction_to(Vector2(cpath[0].x, cpath[0].y))
			move_and_slide(dir * SPEED)
			move = true
			#print('move')
			anim()
			
			if d <= GOAL_DEAD_ZONE and cpath.size() == 1:
				move = false
				cpath = null
				dir = Vector2()
				G.debug_draw_path(self, cpath)
			anim()
		else: # достижение точки
			# удаляем достигнутую точку из пути
			cpath.remove(0)
			# если точек больше нет то достижение цели
			if cpath == PoolVector3Array():
				cpath = null
				move = false
			G.debug_draw_path(self, cpath)
		dir = Vector2()

var cur_point = -1
# "вечный цикл" - процесс
func _physics_process(delta):

	if cpath == null:
		cur_point += 1
		if cur_point > $Path.get_child_count() - 1:
			cur_point = 0
		var pos = spawn_pos + $Path.get_child(cur_point).position
		go_to_point(pos)
	else:
		var pos = spawn_pos + $Path.get_child(cur_point).position
		go_to_point(pos)
		
	#else: # патрулирование (работает не очень т.к юниты застревают в друг друге и в препятствиях)
	#if !move:
	#	patrool_pos = get_tree().get_nodes_in_group('animals')[rand_range(1, len(get_tree().get_nodes_in_group('animals')))].spawn_pos
	#	prints(get_tree().get_nodes_in_group('animals'),patrool_pos)
	#if patrool_pos:
	#	print(patrool_pos)
	#	go_to_point(patrool_pos)
