extends KinematicBody2D
onready var anim_sprite := $"BoneArcherAnimSprite" as AnimatedSprite
onready var health_bar = $"TPHealth"
var ArrowBone = preload("res://player/ArrowBone.tscn")

const GOAL_DEAD_ZONE = 250
const SPEED = 100 # Скорость
var dir = Vector2(0,0) # Направление
var target = null # объект цель
var spawn_pos # точка появления
var cpath # путь (массив
var arch = Vector2()
var move = false #двигается
var go_to_spawn = false # признак возвращения на исходную позицию
export(float) var health = 2 setget set_health, get_health
var dead = false
var patrool_pos
var arching = false
var inv = 0
var iks = 2
var igrek = 2
var knockback = 0
var vul_left = 0
var vul_right = 0
var vul_up = 0
var vul_down = 0
var knockdir = Vector2(0,0)
var motion = 0
var imp = Vector2(0,0)

func _ready():
	health_bar.max_value = health
	health_bar.value = health
	spawn_pos = position
	add_to_group('npcs')
	get_node("../MainCharacter/Camera2D").get_parent().connect("npc_hurt_left", self, "_on_npc_hurt_left")
	get_node("../MainCharacter/Camera2D").get_parent().connect("npc_hurt_right", self, "_on_npc_hurt_right")
	get_node("../MainCharacter/Camera2D").get_parent().connect("npc_hurt_down", self, "_on_npc_hurt_down")
	get_node("../MainCharacter/Camera2D").get_parent().connect("npc_hurt_up", self, "_on_npc_hurt_up")
	get_node("../MainCharacter/Camera2D").get_parent().connect("deinv", self, "_on_deinv")


func set_health(val):
	health = val
	health_bar.value = val
	if health <= 0:
		dead = true
		$OrcDed.play(0.0)
		anim_sprite.play("Death")
		inv=1
		G.score += 20
#		if G.orcsdie == 10:
#			var res = load("res://ui/challengecompl.png")
#			get_node("/root/map/UI/HUD/mission/challenge").visible = false
#			get_node("/root/map/UI/HUD/mission/challengecompl").visible = true
			
			

func get_health():
	return health


func debug_draw_path():
	for p in get_tree().get_nodes_in_group("astar_debug_point"):
			if p.get_meta('master') == self:
				p.queue_free()
	if cpath != null:
		for p in cpath: 
			var s = Sprite.new()
			s.texture = G.astar_dbg_point
			s.position = Vector2(p.x,p.y)
			s.add_to_group('astar_debug_point')
			s.set_meta('master', self)
			get_parent().add_child(s)


func anim():
	if arching == false:
		dir = dir.round()
		if dir == Vector2(-1, 0):
			anim_sprite.play('GoLeft')
		elif dir == Vector2(1, 0):
			anim_sprite.play('GoRight')
		elif dir == Vector2(0, -1):
			anim_sprite.play('GoUp')
		elif dir == Vector2(0, 1):
			anim_sprite.play('GoDown')
		else: # анимация при простое
			if anim_sprite.animation == 'GoLeft':
				anim_sprite.play('StayLeft')
			elif anim_sprite.animation == 'GoRight':
				anim_sprite.play('StayRight')
			elif anim_sprite.animation == 'GoUp':
				anim_sprite.play('StayUp')
			elif anim_sprite.animation == 'GoDown':
				anim_sprite.play('StayDown')


func go_to_point(pos):
	if cpath == null: # путь не существует - создаем
		cpath = G.ASM.get_point_path(
				G.ASM.get_closest_point(Vector3(position.x, position.y, 0)),
				G.ASM.get_closest_point(Vector3(pos.x, pos.y, 0))
		)
		cpath.remove(0) # удаляем точку под своим тайлом, чтобы при начале движения не дрожать
		if cpath == PoolVector3Array():
			cpath = null
		#debug_draw_path()
	else: # путь существует
		var d = floor(position.distance_to(Vector2(cpath[0].x, cpath[0].y))) # floor - округление к меньшему
		#print(d)
		if not(d == 0): # дистанция до ближайщей точки в пути не = 0 // P --{d}-> [o]ooooooooX
			# Движемся к точке
			if target:
				if position.distance_to(target.position) <= GOAL_DEAD_ZONE:
					move = false
					cpath = null
					if arching == false:
						arching = true
						if target.position.x - position.x > abs(target.position.y - position.y):
							anim_sprite.play("ArchRight")
						elif position.x - target.position.x > abs(target.position.y - position.y):
							anim_sprite.play("ArchLeft")
						elif target.position.y - position.y > abs(target.position.x - position.x):
							anim_sprite.play("ArchDown")
						else:
							anim_sprite.play("ArchUp")
					dir = Vector2()
					#debug_draw_path()
				else:
					dir = position.direction_to(Vector2(cpath[0].x, cpath[0].y))
					move_and_slide(dir * SPEED)
					move = true
					#print('move')
					anim()
			else:
				if cpath.size() == 1 and position == spawn_pos:
					move = false
					cpath = null
					go_to_spawn = false
					dir = Vector2()
					#debug_draw_path()
				else:
					dir = position.direction_to(Vector2(cpath[0].x, cpath[0].y))
					move_and_slide(dir * SPEED)
					move = true
					#print('move')
					anim()
				
				
		else: # достижение точки
			# удаляем достигнутую точку из пути
			cpath.remove(0)
			# если точек больше нет то достижение цели
			if cpath == PoolVector3Array():
				cpath = null
				if go_to_spawn:
					go_to_spawn = false
				move = false
			#debug_draw_path()

		dir = Vector2()


# "вечный цикл" - процесс
func _physics_process(delta):
	if target != null and arching == false: # преследование
		#if position.distance_to(target.position) > 16:
			go_to_point(target.position)
		#else:
			#move_and_slide(position.direction_to(target.position))
	else: # нет преследуемого
		if spawn_pos != self.position and arching == false: # возврат на точку
			go_to_point(spawn_pos)
		#else: # патрулирование (работает не очень т.к юниты застревают в друг друге и в препятствиях)
		#	if !move:
		#		patrool_pos = get_tree().get_nodes_in_group('npcs')[rand_range(1, len(get_tree().get_nodes_in_group('npcs')))].spawn_pos
		#	if patrool_pos:
		#		go_to_point(patrool_pos)

func _on_DetectArea_body_shape_entered(body_id, body, body_shape, area_shape):
		#print(body.name)
	if body != null:
		if is_instance_valid(body):
			if body.name == 'MainCharacter':
				target = body

# входящие объекты в область зрения
# выход объект(-а/-ов) из области "зрения"


func _on_DetectArea_body_shape_exited(body_id, body, body_shape, area_shape):
	if body != null:
		if is_instance_valid(body):
			if body.name == 'MainCharacter':
				target = null
				cpath = null
				go_to_spawn = true
				
func _on_npc_hurt_left(bonus_dmg_sword):
	if (self.vul_left==1):
		if inv==0: 
			self.health = self.health - bonus_dmg_sword - 1
			inv = 1
			$BoneHurt.play(0.0)
			iks = -1
			igrek = 0
			knockback = 1
			$KnockbackTimer.start()
			knockdir=Vector2(iks, igrek)
			motion=knockdir.normalized()*500
			move_and_slide(motion, Vector2(0,0))
			anim_sprite.self_modulate = Color.red
			yield(get_tree().create_timer(0.25), "timeout")
			if is_instance_valid(self):
				anim_sprite.self_modulate = Color.white
func _on_npc_hurt_right(bonus_dmg_sword):
	if (self.vul_right==1):
		if inv==0: 
			self.health = self.health - bonus_dmg_sword - 1
			inv = 1
			$BoneHurt.play(0.0)
			iks = 1
			igrek = 0
			knockback = 1
			$KnockbackTimer.start()
			knockdir=Vector2(iks, igrek)
			motion=knockdir.normalized()*500
			move_and_slide(motion, Vector2(0,0))
			anim_sprite.self_modulate = Color.red
			yield(get_tree().create_timer(0.25), "timeout")
			if is_instance_valid(self):
				anim_sprite.self_modulate = Color.white

func _on_npc_hurt_down(bonus_dmg_sword):
	if (self.vul_down==1):
		if inv==0: 
			self.health = self.health - bonus_dmg_sword - 1
			inv = 1
			$BoneHurt.play(0.0)
			iks = 0
			igrek = 1
			knockback = 1
			$KnockbackTimer.start()
			knockdir=Vector2(iks, igrek)
			motion=knockdir.normalized()*500
			move_and_slide(motion, Vector2(0,0))
			anim_sprite.self_modulate = Color.red
			yield(get_tree().create_timer(0.25), "timeout")
			if is_instance_valid(self):
				anim_sprite.self_modulate = Color.white

func _on_npc_hurt_up(bonus_dmg_sword):
	if (self.vul_up==1):
		if inv==0: 
			self.health = self.health - bonus_dmg_sword - 1
			inv = 1
			$BoneHurt.play(0.0)
			iks = 0
			igrek = -1
			knockback = 1
			$KnockbackTimer.start()
			knockdir=Vector2(iks, igrek)
			motion=knockdir.normalized()*500
			move_and_slide(motion, Vector2(0,0))
			anim_sprite.self_modulate = Color.red
			yield(get_tree().create_timer(0.25), "timeout")
			if is_instance_valid(self):
				anim_sprite.self_modulate = Color.white
func _on_deinv():
	inv = 0

func r(d,k):
	var i=0
	var a=d
	var deg = d
	while  abs(a)>0.0001:
		a = a*(d*d*(2*i+1)*(2*i+1)/(2*(i+1)*(2*i+3)))
		deg+=a;
		i+=1;
	if k >=0: return deg
	else: return 3.14159265-deg
	pass

func _on_BoneArcherAnimSprite_animation_finished():
	arching = false
	if anim_sprite.animation=="Death":
		queue_free() # Replace with function body.



func _on_KnockbackTimer_timeout():
	knockback = 0


func _on_BoneArcherAnimSprite_frame_changed():
	if $BoneArcherAnimSprite.animation.match("Arch*") && $BoneArcherAnimSprite.frame==7:
		imp = Vector2(G.Player.position.x-position.x,G.Player.position.y-position.y).normalized()
	if $BoneArcherAnimSprite.animation.match("Arch*") && $BoneArcherAnimSprite.frame==9:
		var arr = ArrowBone.instance()
		arr.position = position
		get_parent().add_child(arr)
		arr.rotate(r(imp.y,imp.x))
		arr.apply_impulse(Vector2(),imp*300)
