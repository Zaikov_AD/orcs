extends Node

# F4 - debug

signal upd_path

signal npc_get_vul_down
signal npc_get_inv_down
signal npc_get_vul_up
signal npc_get_inv_up
signal npc_get_vul_left
signal npc_get_inv_left
signal npc_get_vul_right
signal npc_get_inv_right
signal npc_get_shot
signal player_hurt
signal destroy


onready var ASM := AStar.new() as AStar
var HUD
var UI
var DBG
var Player
var current_map
var score = 0 setget set_score, get_score
const VOID_TILE = -1
var debug = false
var save_lvl
var astar_dbg_point = preload("res://debug/apoint.png")

var cur_stats = {"H":0 , "D":0 , "S": 0}


# Нвбор разрешений
enum Permit {
	sword,
	arch,
	left,
	right,
	up,
	down,
	water,
	fire,
	earth,
	heal
}

# Текущие разрешения
var permissions = {
	Permit.sword : false,
	Permit.arch : false,
	Permit.left : false,
	Permit.right : false,
	Permit.up : false,
	Permit.down : false,
	Permit.water : false,
	Permit.fire : false,
	Permit.earth : false,
	Permit.heal : false
}

const BASE_NPC_DMG = 1
const BASE_SCORE_INC = 20 # кол-во очков опыта за убийство НПС

func save_game(cur_lvl_index):
	var sg = File.new()
	sg.open("user://savegame.save", File.WRITE)
	sg.store_line(str(cur_lvl_index))
	sg.close()

func load_game():
	var sg = File.new()
	if not sg.file_exists("user://savegame.save"):
		return # Error! We don't have a save to load.
	sg.open("user://savegame.save", File.READ)
	while not sg.eof_reached():
		return int(sg.get_line())
	sg.close()

func _init_UI():
	if !get_tree().root.has_node('UI'):
		UI = CanvasLayer.new()
		UI.name = "UI"
		HUD = load("res://ui/HUD.tscn").instance()
		UI.add_child(HUD)
		get_tree().root.call_deferred("add_child", UI)


func reset_permissions():
	for key in permissions.keys():
		permissions[key] = false


func create_graph_for_astar(A, tmaps, ignore_tiles):
	var idp = 0
	#water - 2, 6 
	ignore_tiles[tmaps[0]] = ignore_tiles[tmaps[0]] + [VOID_TILE]
	#print(ignore_tiles)
	# Создание вершин для AStar
	for i in tmaps[0].get_used_rect().size.x:
		for j in tmaps[0].get_used_rect().size.y:
			if (not(tmaps[0].get_cell(i, j) in ignore_tiles[tmaps[0]]) 
				and not(tmaps[1].get_cell(i, j) in ignore_tiles[tmaps[1]])
				and not(tmaps[2].get_cell(i, j) in ignore_tiles[tmaps[2]])):
				var wpos = tmaps[0].map_to_world(Vector2(i,j)) + Vector2(16, 16)
				#A.add_point(idp, Vector3(wpos.x, wpos.y, 0))
				A.add_point(idp, Vector3(wpos.x, wpos.y, 0))
				idp += 1
	# Создание соединений 
	for i in tmaps[0].get_used_rect().size.x:
		for j in tmaps[0].get_used_rect().size.y:
			if (not(tmaps[0].get_cell(i, j) in ignore_tiles[tmaps[0]]) 
				and not(tmaps[1].get_cell(i, j) in ignore_tiles[tmaps[1]])
				and not(tmaps[2].get_cell(i, j) in ignore_tiles[tmaps[2]])):
				var wpos = tmaps[0].map_to_world( Vector2(i, j)) + Vector2(16, 16)
				var id_cp = A.get_closest_point(Vector3(wpos.x, wpos.y, 0))
				#
				var id_npx = A.get_closest_point( Vector3(wpos.x + 32, wpos.y, 0) )
				#
				var id_npy = A.get_closest_point( Vector3(wpos.x, wpos.y + 32, 0) )
				if A.get_point_position(id_cp).y == A.get_point_position(id_npx).y: # запрет диагоналей
					A.connect_points(id_cp, id_npx)

				if A.get_point_position(id_cp).x == A.get_point_position(id_npy).x: # запрет диагоналей
					A.connect_points(id_cp, id_npy)


func get_ignore_tiles(tmaps):
	var ignore_tiles = {}
	for tmap in tmaps: 
		ignore_tiles[tmap] = []
		for tid in tmap.tile_set.get_tiles_ids():
			if tmap.tile_set.tile_get_shape_count(tid) > 0:
				ignore_tiles[tmap].append(tid)
	return ignore_tiles
#
func debug_draw_path(npc, cpath):
	if debug:
		for p in get_tree().get_nodes_in_group("astar_path_point"):
				if p.get_meta('master') == npc:
					p.queue_free()
		if cpath != null:
			for p in cpath: 
				var s = Sprite.new()
				s.texture = astar_dbg_point
				s.position = Vector2(p.x, p.y)
				s.add_to_group('astar_path_point')
				s.set_meta('master', npc)
				DBG.add_child(s)


func debug_astar_vecrtexes(A, tmap):
	for pid in A.get_points():
		var s = Sprite.new()
		s.texture = astar_dbg_point
		s.self_modulate = Color.aqua
		s.scale = Vector2(0.5, 0.5)
		var pos = A.get_point_position(pid)
		s.position = Vector2(pos.x, pos.y) #tmap_l1.map_to_world(Vector2(i,j))
		s.add_to_group("astar_walk_points")
		DBG.add_child(s)

#
func _process(delta):
	if Input.is_action_just_pressed("debug"):
		if debug:
			for p in get_tree().get_nodes_in_group("astar_path_point"):
				p.queue_free()
			for p in get_tree().get_nodes_in_group("astar_walk_points"):
				p.queue_free()
				debug = false
		else:
			if current_map != null:
				if current_map.has_node("YSort/firstLayer"):
					debug_astar_vecrtexes(ASM, current_map.get_node("Sort/firstLayer"))
					debug = true


func _ready():
	var debug = Node2D.new()
	debug.z_index = 1
	debug.name = "debug"
	DBG = debug
	get_tree().root.call_deferred("add_child", DBG)
	# инициализация игрового интерфейса
	_init_UI()
	# сигнал урона по НПС
	connect("player_get_dmg", self, "_on_dmg_for_player")
	connect("npc_get_vul_down", self, "_on_vul_for_npc_down")
	connect("npc_get_inv_down", self, "_on_inv_for_npc_down")
	connect("npc_get_vul_up", self, "_on_vul_for_npc_up")
	connect("npc_get_inv_up", self, "_on_inv_for_npc_up")
	connect("npc_get_vul_left", self, "_on_vul_for_npc_left")
	connect("npc_get_inv_left", self, "_on_inv_for_npc_left")
	connect("npc_get_vul_right", self, "_on_vul_for_npc_right")
	connect("npc_get_inv_right", self, "_on_inv_for_npc_right")
	connect("npc_get_shot", self, "_on_npc_get_shot")

func _on_npc_get_shot(npc, dmg):
	npc.health -= dmg
	npc.emit_signal("hurt")
	npc.anim_sprite.self_modulate = Color.red
	npc.target = Player
	yield(get_tree().create_timer(0.25), "timeout")
	if is_instance_valid(npc):
		npc.anim_sprite.self_modulate = Color.white
				
func _on_vul_for_npc_down(body):
	body.vul_down = 1
func _on_inv_for_npc_down(body):
	body.vul_down = 0
func _on_vul_for_npc_up(body):
	body.vul_up = 1
func _on_inv_for_npc_up(body):
	body.vul_up = 0
func _on_vul_for_npc_left(body):
	body.vul_left = 1
func _on_inv_for_npc_left(body):
	body.vul_left = 0
func _on_vul_for_npc_right(body):
	body.vul_right = 1
func _on_inv_for_npc_right(body):
	body.vul_right = 0



# слот для сигнала "обновить путь"
func _on_upd_path():
	for npc in get_tree().get_nodes_in_group('npcs'):
		if npc.target != null:
			npc.cpath = null

# Установить счет
func set_score(val):
	score = val # в переменную
	if is_instance_valid(HUD.lscore):
		HUD.lscore.text = str(score).pad_zeros(4) #установить визуально 0000

# получить счет (int)
func get_score() -> int:
	return score

# Добавить счет
func add_score(val):
	score += val
	HUD.lscore.text = str(score)
	
