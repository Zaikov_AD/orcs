extends Area2D

var ready_to_annihilate = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	$MagicSprite.play("annihilate")



func _on_CircleOfDeath_body_entered(body):
	if body.name=='MainCharacter':
		ready_to_annihilate=1
	


func _on_CircleOfDeath_body_exited(body):
	if body.name=='MainCharacter':
		ready_to_annihilate=0


func _on_MagicSprite_animation_finished():
	queue_free()


func _on_MagicSprite_frame_changed():
	if $MagicSprite.frame==16:
		$LightningStrike.play(0.0)
	if $MagicSprite.frame>15 &&  $MagicSprite.frame<20 && ready_to_annihilate==1:
		G.emit_signal("destroy")
