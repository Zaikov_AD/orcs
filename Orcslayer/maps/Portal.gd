extends Sprite

func _ready():
	var timer= Timer.new()
	timer.connect("timeout",self,"tick")
	add_child(timer)
	timer.wait_time = 0.25
	timer.start()

func tick():
	if frame < 20 or (frame < 44 and frame > 38):
		frame+=1
	else: frame -=5
