extends Area2D

func _on_Area2D_body_shape_entered(body_id, body, body_shape, area_shape):
	if body == G.Player :
		G.HUD.cd_go_to_scene.show()

func _on_Area2D_body_shape_exited(body_id, body, body_shape, area_shape):
	if body == G.Player:
		G.HUD.cd_go_to_scene.hide()