extends Sprite

var timer

func _ready():
	show_on_top = true
	timer = Timer.new()
	timer.connect("timeout",self,"tick")
	add_child(timer)
	timer.wait_time = 0.5
	timer.start()

func tick():
	if self.frame < 3:
		self.frame = self.frame + 1
	else:
		self.frame = 0

#	pass
