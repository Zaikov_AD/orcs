extends Node2D

onready var tmap_l1 = $"YSort/firstLayer"
onready var tmap_l2 = $"YSort/secondLayer"
onready var tmap_l3 = $"YSort/Block"

var ignore_tiles = {}


func _ready():
	print(name)
	G.HUD.visible = true
	G.current_map = self
	ignore_tiles = G.get_ignore_tiles([tmap_l1, tmap_l2, tmap_l3])
	G.create_graph_for_astar(G.ASM, [tmap_l1, tmap_l2, tmap_l3], ignore_tiles)
	get_tree().paused = true
	$Music.play(0.0)
	
"""
var tmp_targets = []


func _input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed:
			print('lkm')
			match tmp_targets.size():
				1:
					tmp_targets.append(get_global_mouse_position())
					var path = G.ASM.get_point_path(  
						G.ASM.get_closest_point(Vector3(tmp_targets[0].x,tmp_targets[0].y,0)),
						G.ASM.get_closest_point(Vector3(tmp_targets[1].x,tmp_targets[1].y,0))
					)
					print(path)
					for p in path: 
						var s = Sprite.new()
						s.texture = G.astar_dbg_point
						s.position = Vector2(p.x,p.y)
						tmap_l1.add_child(s)
						tmp_targets.clear()
				0: 
					for ch in tmap_l1.get_children():
						ch.queue_free()
					tmp_targets.append(get_global_mouse_position())

	#print(G.ASM.get_point_path(1,10))
"""
