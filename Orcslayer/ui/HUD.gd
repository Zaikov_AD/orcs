extends Control

#UI - user interface 

onready var select = $HBox_for_select
onready var selected = $HBox_selected
onready var lives = $VBoxStats/HBoxLives
onready var lscore = $VBoxStats/Score
onready var cd_go_to_scene = $"ConfirmDialog"
onready var dead_scr = $DeadScreen
onready var market = $Market

var checked = false

func reset_actions():
	select.visible = true
	$"back".visible = true
	$"TextIN".visible = true
	$"TextIN2".visible = true
	$"TextIN3".visible = true
	$"TextIN4".visible = true
	$"TextIN5".visible = true
	$"VideoPlayer".visible = true
	$"VideoPlayer2".visible = true
	for ch in selected.get_children():
		_on_TB_pressed(ch)


func _check_rule():
	if (selected.get_child_count() == 5 
			and (G.permissions[G.Permit.left] or G.permissions[G.Permit.right]) 
			and (G.permissions[G.Permit.down] or G.permissions[G.Permit.up])):
		select.visible = false
		for ch in selected.get_children():
			ch.disconnect('pressed', self, '_on_TB_pressed')
		return true
	return false


func _on_TB_pressed(button: TextureButton):
	var mb := button.get_meta('master') as TextureButton
	mb.pressed = false
	mb.disabled = false
	button.queue_free()


func _on_TB_toggled(button_pressed: bool, button: TextureButton):
	if button_pressed:
		if selected.get_child_count() < 5:
			var tb = TextureButton.new() # кнопка для панели с выбранными
			tb.name = button.name#'TB' + str(selected.get_child_count() + 1)
			tb.texture_normal = button.texture_normal
			tb.expand = true
			tb.rect_min_size = button.rect_min_size / 2
			tb.set_meta('master', button)
			(tb as TextureButton).connect("pressed", self, "_on_TB_pressed", [tb])
			selected.add_child(tb)
			
			var m = []
			for ch in selected.get_children():
				m.append(ch.name)
			m.sort()
			var i = 0
			for n in m:
				selected.move_child(selected.get_node(n), i)
				i += 1
				
			button.disabled = true
		else:
			button.pressed=false

	#if button_pressed:z
	#	if selected.get_child_count() < 5:
	#		select.remove_child(button)
	#		selected.add_child(button)
	#else:
	#	selected.remove_child(button)
	#	select.add_child(button)

func reset_lives():
	for ch in lives.get_children():
		ch.queue_free()
	
func minus_live_indicator():
	if lives.get_child_count() > 0:
		lives.remove_child(lives.get_child(lives.get_child_count() - 1))


func add_live_indicator():
	var live = TextureRect.new()
	live.texture = load('res://ui/img/heart.png')
	live.expand = true
	live.rect_min_size = Vector2(32, 32)
	live.rect_size = Vector2(32, 32)
	lives.add_child(live)


func _ready():
	for ch in select.get_children():
		(ch as TextureButton).connect("toggled", self, "_on_TB_toggled", [ch])


# кнопка OK
func _on_btn_ok_pressed():
	G.reset_permissions()
	for ch in selected.get_children():
		var p = ch.get_meta('master').permit
		G.permissions[p] = true
	checked = _check_rule()
	if checked:
		#print(G.permissions)
		$"btn_ok".visible = false
		get_tree().paused = false
		$"back".visible = false
		$"TextIN".visible = false
		$"TextIN2".visible = false
		$"TextIN3".visible = false
		$"TextIN4".visible = false
		$"TextIN5".visible = false
		$"VideoPlayer".visible = false
		$"VideoPlayer2".visible = false
		G.current_map.get_node("YSort/MainCharacter").start()


# респавн
func _on_BtnRespawn_pressed():
	G.Player.reset()
	get_tree().call_group("npcs", "reset")
	dead_scr.visible = false
	get_tree().paused = false

	
# приняли переход на следующий уровень
func _on_ConfirmDialog_confirmed():
	G.disconnect("upd_path", G, "_on_upd_path") 
	var cur_lvl_index = int(G.current_map.name.split('_')[1])
	G.save_game(cur_lvl_index + 1)
	if(cur_lvl_index == 1):
		get_tree().change_scene("res://maps/map3.tscn")
	else:
		get_tree().change_scene("res://maps/map2.tscn")
	G.HUD.market.show()
	$"btn_ok".visible = true
	reset_actions()