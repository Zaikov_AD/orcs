extends Control

const PRICE_STEP = 20

var score = 0 setget set_score, get_score
onready var PB_H = $"CR_bg/TextureRect/CenterContainer/VBoxContainer/GridContainer/ProgressBar_H"
onready var PB_D = $"CR_bg/TextureRect/CenterContainer/VBoxContainer/GridContainer/ProgressBar_D"
onready var PB_S = $"CR_bg/TextureRect/CenterContainer/VBoxContainer/GridContainer/ProgressBar_S"
onready var LScore = $"CR_bg/TextureRect/CenterContainer/VBoxContainer/LScore"

func set_score(val):
	score = val
	LScore.text = tr("[SCORE]") + ": " + str(score).pad_zeros(4)
	
func get_score():
	return score
	
func show():
	self.score = G.get_score()
	.show()
	get_tree().paused = true

func test():
	G.score = 100
	
func _ready():
	self.score = G.get_score()
		
# кнопка ОК - принять
func _on_Button_pressed():
	visible = false
	# сохраняем характеристики
	G.cur_stats["H"] = PB_H.value
	G.cur_stats["D"] = PB_D.value
	G.cur_stats["S"] = PB_S.value


# сброс к значениям до изменения
func _on_Btn_reset_pressed():
	PB_H.value = G.cur_stats["H"] 
	PB_D.value = G.cur_stats["D"] 
	PB_S.value = G.cur_stats["S"]
	self.score = G.get_score()


func _on_Btn_AddHealth_pressed():
	if PB_H.value != PB_H.max_value and score - ((PB_H.value + 1) * PRICE_STEP) >= 0:
		PB_H.value += 1
		self.score -= PB_H.value * PRICE_STEP


func _on_Btn_AddDamage_pressed():
	if PB_D.value != PB_D.max_value and score - ((PB_D.value + 1) * PRICE_STEP) >= 0:
		PB_D.value += 1
		self.score -= PB_D.value * PRICE_STEP


func _on_Btn_AddSpeed_pressed():
	if PB_S.value != PB_S.max_value and score - ((PB_S.value + 1) * PRICE_STEP) >= 0:
		PB_S.value += 1
		self.score -= PB_S.value * PRICE_STEP
